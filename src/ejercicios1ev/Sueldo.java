package ejercicios1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sueldo {
	
	float sueldo = 0;
	int ventas = 3;
	int comision = 10;
	float totalVentas = 0;
	float totalNeto = 0;
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        String entrada = null;
        Sueldo s = new Sueldo();
        
        try {
			System.out.println("-- CALCULAR SUELDO CON COMISIONES --");
			System.out.print("Introduce sueldo: ");
			entrada = reader.readLine();
			s.sueldo = Float.parseFloat(entrada);
			
			System.out.print("Introduce venta 1: ");
			entrada = reader.readLine();
			s.totalVentas += Float.parseFloat(entrada);
			
			System.out.print("Introduce venta 2: ");
			entrada = reader.readLine();
			s.totalVentas += Float.parseFloat(entrada);
			
			System.out.print("Introduce venta 3: ");
			entrada = reader.readLine();
			s.totalVentas += Float.parseFloat(entrada);
			
			s.totalNeto = s.sueldo + (s.totalVentas * s.comision / 100);
			System.out.println("TOTAL A PERCIBIR: " + s.totalNeto);
      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

}
