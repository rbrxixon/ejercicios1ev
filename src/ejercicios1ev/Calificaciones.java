package ejercicios1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Calificaciones {
	
	String[] materiasArray = {"MATEMATICAS","FISICA","QUIMICA"};
	int[] examenCriterioArray = {90,80,85};
	int[] tareasCriterioArray = {10,20,15};
	int[] numeroTareasArray = {3,2,3};
	
	float[] examenCalificacionArray = null; 
	float[] tareasCalificacionArray = null;
	float[] examenPromedioArray = null;
	float[] tareasPromedioArray = null;
	float[] calificacionFinalMateriaArray = null;
	float calificacionFinal = 0;
	float calificacionFinalSuma = 0;
	DecimalFormat df = null;
	
	public static void main(String[] args) {
		
		Calificaciones c = new Calificaciones();
		c.examenCalificacionArray = new float[3];
		c.tareasCalificacionArray = new float[3];
		c.examenPromedioArray = new float[3];
		c.tareasPromedioArray = new float[3];
		c.calificacionFinalMateriaArray = new float[3];
		c.df = new DecimalFormat("0.00");
		c.df.setMaximumFractionDigits(2);
		
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        
        String entrada = null;
        try {
			System.out.println("-- CALIFICACIONES MATERIAS --");
			
			for (int i = 0; i < c.materiasArray.length; i++) {
				System.out.println("*** MATERIA: " + c.materiasArray[i] + " ***");
				System.out.println("EXAMEN: " + c.examenCriterioArray[i] + "% | TAREAS: " + c.tareasCriterioArray[i] + "% | Nº TAREAS REALIZADAS: " + c.numeroTareasArray[i]);
				System.out.print("Introduce calificacion del examen: ");
				entrada = reader.readLine();
				c.examenCalificacionArray[i] = Float.parseFloat(entrada);
				
				for (int j = 0; j < c.numeroTareasArray[i]; j++) {
					System.out.print("Introduce calificacion de la tarea nº " + (j+1) + ": ");
					entrada = reader.readLine();
					c.tareasCalificacionArray[i] += Float.parseFloat(entrada);
				}
				c.tareasPromedioArray[i] = ((c.tareasCalificacionArray[i] / c.numeroTareasArray[i]) * c.tareasCriterioArray[i] / 100);
				c.examenPromedioArray[i] = ((c.examenCalificacionArray[i] * c.examenCriterioArray[i]) / 100);
				
				//System.out.println("Promedio tareas: " + c.tareasPromedioArray[i]);
				//System.out.println("Promedio examen: " + c.examenPromedioArray[i]);
				c.calificacionFinalMateriaArray[i] = c.tareasPromedioArray[i] + c.examenPromedioArray[i];
				System.out.println("PROMEDIO MATERIA: " + c.df.format(c.calificacionFinalMateriaArray[i]));
			}
			
			for (int i = 0; i < c.calificacionFinalMateriaArray.length; i++) {
				c.calificacionFinalSuma += c.calificacionFinalMateriaArray[i];
			}
			
			c.calificacionFinal = (c.calificacionFinalSuma / c.materiasArray.length);
			System.out.println("PROMEDIO DE TODAS LAS MATERIAS: " + c.df.format(c.calificacionFinal));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
  
        
	}
}