package ejercicios1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cronometro {
    private long startTime = 0;
    private long stopTime = 0;
    
    public Cronometro() {
    	startTime = System.currentTimeMillis();
    }
    
    public void stop() {
        stopTime = System.currentTimeMillis();
    }
    
    public double getElapsedTimeSecs() {
        double elapsed;
        elapsed = ((double)(stopTime - startTime)) / 1000;
        return elapsed;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	    String entrada = null;
        BufferedReader reader = null;
               
		try {
			Cronometro cronometro = new Cronometro();
			reader = new BufferedReader(new InputStreamReader(System.in)); 
			System.out.print("Dime tu nombre: ");
			entrada = reader.readLine();
			cronometro.stop();
			System.out.println("Hola " + entrada + ", has tardado " + cronometro.getElapsedTimeSecs() + " segundos en decirme tu nombre");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}


