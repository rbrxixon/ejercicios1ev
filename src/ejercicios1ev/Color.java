package ejercicios1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Color {

	int r, g, b = 0;
	double y, i, q = 0;
	
	
	public static void main(String[] args) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in)); 
        String entrada = null;
        Color c = new Color();
        
        try {
			System.out.println("-- CONVERTIR RGB A YIQ --");
			System.out.print("Introduce valor rojo (r): ");
			entrada = reader.readLine();
			c.r = Integer.parseInt(entrada);
			
			System.out.print("Introduce valor verde (g): ");
			entrada = reader.readLine();
			c.g = Integer.parseInt(entrada);
			
			System.out.print("Introduce valor azul (b): ");
			entrada = reader.readLine();
			c.b = Integer.parseInt(entrada);
			
			//y=0,299r+0,587 g+0,114 b
			//i=0,596 r−0,275 g−0,321b
			//q=0,212 r−0,528 g+0,311b
			c.y = ((0.299 * c.r) + (0.587 * c.g) + (0.114 * c.b));
			c.i = ((0.596 * c.r) - (0.275 * c.g) - (0.321 * c.b));
			c.q = ((0.212 * c.r) - (0.528 * c.g) + (0.311 * c.b));
			
			System.out.println("R: " + c.r + "  | G: " + c.g + " | B: " + c.b);
			System.out.println("Y: " + c.y + "  | I: " + c.i + " | Q: " + c.q);
		      
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
