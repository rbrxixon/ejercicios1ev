package ejercicios1ev;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Conversor {

	public static void main(String[] args) {
		// Enter data using BufferReader 
        BufferedReader reader =  
                   new BufferedReader(new InputStreamReader(System.in)); 
         
        // Reading data using readLine 
        String entrada = null;
        Float euros = null;
        Float dolares = null;
        float valorDolar = 1.19f;
        DecimalFormat df = null;
        
		try {
			System.out.println("-- CONVERSOR DE EUROS A DOLARES --");
			System.out.println("EUROS: ");
			entrada = reader.readLine();
			euros = Float.parseFloat(entrada);
			dolares = euros * valorDolar;
			
			df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
  
        // Printing the read line 
        //System.out.println("Entrada: " + entrada);
        System.out.println("1 Euro = 1,19 Dolares");  
        System.out.println("Euros: " + euros + " €"); 
        System.out.println("Dolares: " + df.format(dolares) + " $");  
	}

}
